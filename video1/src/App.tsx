import { styled } from 'goober'

/* Components' Imports */
import Card from './random'

const Container = styled('div')`
    
`

function App(): JSX.Element {

    const data: { title: string, description: string, img: string }[] = [
        { title: 'Integraciones con aplicaciones', description: 'Conecta Asana con tus herramientas favoritas como Slack, Gmail y Outlook.', img: 'https://d3ki9tyy5l5ruj.cloudfront.net/obj/fc89d79516dcec7d8da761d533c112879ce22f4f/apps.svg' },
        { title: 'Recomendado (casos de uso)', description: 'Mira cómo otros equipos como el tuyo gestionan sus flujos de trabajo en Asana.', img: 'https://d3ki9tyy5l5ruj.cloudfront.net/obj/efa540cc7c79988a10f07bbd3b562947d01d85f5/presentation_screen.svg' },
        { title: 'Capacitación en vivo', description: 'Recibe consejos del equipo de Éxito del Cliente de Asana en tiempo real.', img: 'https://d3ki9tyy5l5ruj.cloudfront.net/obj/78dfe46769838a5801af9c9c4a0858fa98e0e904/person_with_asana_screen.svg' },
        { title: 'Academia Asana', description: 'Aprende a tu propio ritmo con nuestros tutoriales, videos y capacitaciones.', img: 'https://d3ki9tyy5l5ruj.cloudfront.net/obj/6e0bddbef4aac61b69664bea06fc44e60097c11c/graduation_cap.svg' }
    ]

    return (

        <Container>

            {data.map((v, i) => <Card
                key={i}
                title={v.title}
                description={v.description}
                img={v.img} />
            )}

        </Container>

    )
}

export default App
