import { styled } from 'goober'

const Container = styled('div')`

    box-sizing: border-box;
    background-color: #363639;
    height: 25vh;
    padding: 30px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;

    .body {
        display: flex;
        min-height: 100px;
        width: 25rem;
        justify-content: center;
        align-items: center;
        gap: 10px;
        padding: 20px 10px;
        border-radius: 10px;
        border: 1px solid #464649;
    }

    .text-container {
        display: flex;
        flex-direction: column;
        gap: 5px;
    }

    .title {
        color: white;
        font-weight: bold;
        text-transform: capitalize;
    }

    .description {
        color: grey;
        font-size: 14px;
    }

`

export interface params {
    img: string,
    title: string,
    description?: string
}

function App(params: params): JSX.Element {

    return (

        <Container className='container'>
            <div className='body'>
                <img src={params.img} alt="profile-picture" />
                <div className='text-container'>
                    <p className='title'>{params.title}</p>
                    <p className='description'>{params.description}</p>
                </div>
            </div>
        </Container>

    )
}

App.defaultProps = {
    img: 'vite.svg'
}

export default App